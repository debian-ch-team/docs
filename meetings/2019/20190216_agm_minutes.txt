-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Minutes
*********

Presence: Didier Raboud (OdyX, chair), Philipp Hug, Gaudenz Steinlin
(minutes), Axel Beckert

Excused: Eva Ramon, Daniel Pocock, Iustin Pop, Raphael Walther

Date: February 16, 2019 17:00-18:18

* Greetings
  Voting members present: 4

* Ratification of the minutes of the AGM 2018
  Agreed: 4

* Reports from the board

** President (Didier Raboud)
   The mailinglist was migrated to Debian infrastructure. There was a
   BSP in December in Bern with reasonable attendance. The regular
   meetups in Zurich are still active and mostly well attended.
   Otherwise not much activity this year. Only small (~30 CHF)
   merchandise sales.

** Treasurer (Philipp Hug)
   Three larger donations and several smaller ones. The total amount of
   donations is 5923.65 CHF, the total profit is 4334.74 CHF. The
   detailed financial statement is attached to the minutes.
   Agreed: 3
   Abstain: 1

* Discharge of the board
  Agreed: 4

* Approval of new members
  no new members applied since the last AGM

* Board election
  Candidates: Didier Raboud, Philipp Hug, Gaudenz Steinlin
  Agreed: 4

* Varia

** Raphael has a Debian banner from DebConf 13
   We decide it should go to someone who can use it better. We decided
   that it should go to Axel Beckert at the last AGM but that did not
   happen. Didier will check with him.

** @debian.ch Mail aliases: Philipp will take care to try to move them
   to Debian infrastructure. If that's not possible he will try to
   find another solution even a commercial one in Switzerland.

** debian.ch Website: We move the website to be hosted at netlify or
   another CDN. Philipp will implement a solution which pushes there
   from our repository on salsa.debian.org.

** Donations in Bitcoin: We decided to accept them last year, but it's
   not yet announced on the website. Philipp will take care.

** Mini-Debconf in Vaumarcus: We are investigating the possibility of
   a Mini-Debconf on the Week-end of September 20/21/22 or any other
   date in September/October. Didier will talk with Le Camp about
   possible dates and take the lead in organization.

** Next AGM: 15 February 2020 in Zurich. Organizer: Axel Beckert
-----BEGIN PGP SIGNATURE-----

iHUEARYKAB0WIQS0SHD2ODl+Y8zt8TtkDjbn9vx/EgUCXrB8UgAKCRBkDjbn9vx/
EmAUAQCI7RtB3L5+vdYFQFfqUAFFaTvWmQjZ532mdmiG+r+mvwD/ZYTrNJSPpUIe
pA+tdOL1qyzXrcHNsTg0Ol5V1JJp6gc=
=eDWM
-----END PGP SIGNATURE-----
