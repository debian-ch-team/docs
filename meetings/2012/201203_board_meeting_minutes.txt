
debian.ch Board meeting by Email - Minutes 
******************************************

Time: March 2012

This debian.ch Board meeting was held during March 2012 by email. The 
following decisions were taken:

** Board organization
   The following positions were assigned to board members:
   - President: Luca Capello
   - Treasurer: Philipp Hug
   - Secretary: Gaudenz Steinlin

** Adresses
   Postal adress of the association (as published on the website):
   debian.ch
   c/o Gaudenz Steinlin
   Donnerbühlweg 33
   3012 Bern

   All communication of Postfinance should directly go to the treasurer.
   The communication adress for Postfinance will be changed to:
   Philipp Hug
   Stockenerstrasse 60
   8405 Winterthur


The secretary



Gaudenz Steinlin
