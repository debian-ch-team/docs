-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512


debian.ch General Meeting 2012 - Minutes
****************************************

Location: Donnerbühlweg 33, 3012 Bern [1]
Time:     23/04/2012 19:15 - ca. 22:30

Attendees: Luca Capello (president), Michele Cane, Philipp Hug,
           Michael Germini, Marc Fournier, Giacomo Catenazzi, Axel
           Beckert, Salvatore Bonacorso, Marcus Möller, Guido Trotter
           (guest), Iustin Pop, Didier Raboud, Gaudenz Steinlin
           (minutes), Daniel Pocock

* Greeting

Luca greets everyone.

* Ratification of the minutes of the AGM 2011 [3]

4 yes, 0 no, rest abstains because they were not there

* Reports from the board: 

 - report from the president Not so much activity this year. We sold 
   quite a lot of merchandise at FOSDEM, DebConf, Open Source Now and 
   other events. 
   In the second half of the year starting from DebConf there were several
   meetings for the DebConf 13 in Switzerland bid. The bid was accepted 
   last Monday. Luca gave two talks at "Open Source Now" about Debian and 
   debian.ch.

 - financial report
   Philipp Hug presents the financial report. We "won" about 2514.94. 
   [2011.12.31-Accounts-2011.pdf]

   merchandise stock:
   + quite a lot of umbrellas (~200)
   + debian.ch T-shirts (classic) (~70)
   + mostly big and small size anarchism T-shirts (~20)
   + some debian Brazil T-shorts (~30)
   + debian swiss army knives (~45)
   [[ luca produces a list with exact numbers ]]

   The report is unanimously agreed upon.

* Discharge of the board
    2 abstain, 7 yes

* Members:
  - new members in 2011: Salvatore Bonaccorso and Philipp Hug, as 
    both are DDs no voting is required.
  - proposed new member: Michele Cane, Marcus Möller, Michael Germini, 
                         Guido Trotter, George Danchev, Daniel Pacock
    They were all accepted by acclamation.

* Board:
  - elections for 2012, Candidates: Luca Capello (president), 
    Gaudenz Steinlin (secretary), Philipp Hug (trasurer).
    Voted by acclamation. (The 3 candidates abstain).

* Bylaws:

  - There was quite some discussion wether the bylaws should be
    translated to english or not and if the english version should be
    the only one, a legally binding translation or just an informal
    translation. At the end a vote about the following resolutions was
    taken:

    * Translation to english should be done 
      => 12 yes, 0 no, 2 abstain
    * Should we spend money for the translation: 
      => 5 yes, 6 no, 3 abstain  
    * Should we spend money on the translation if it could become
      legally binding:
      => 10 yes, 3 no, 1 abstain
    * Budget of CHF 250.- for the translation:
      => 9 yes, 4 no, 1 abstain
      
    ==>> The final resolution is that we would like to have an english
         translation, but that money should only be spent if it's
         possible to have them legally binding. In this case a budget
         of CHF 250.- was approved.

  - Marcus Möller will investigate if we can have english bylaws.

* Merchandise:
    Luca is looking for someone to help doing all the merchandise
    stuff. We are not really earning much on the merchandise.
 
    New planned productions in 2012:
      - New batch of stickers (luca)
      - New batch of fusion t-shirts (luca)

* Misc:
  - Move of debian.ch services to debian.org infrastructure
    Philipp manges this project he has the authority to decide how to
    do it. The move away from Diana's server should be completed
    within 3 months. What we would like to have:
     - Keep debian.ch
     - Private mailinglists and git repositories

  - Debconf13.ch status
    The debian.ch proposal got accepted on Monday. Hurrah!

  - Proposals for activities:
       + mini debconf together with GULL (Michael)
       + BSP (Luca)




The secretary:



Gaudenz Steinlin
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQEVAwUBT22sGU0yN7tZsYcyAQoJtQgAkDYpjpkRYe/CrbCt7Gf+95lCS0vFKAhj
SEgE+Q4S8dbLULPofHHpxA3a8wi1uGktmX129QNRww5NSQyn04L7bcoEBlf/sAKd
KTMv2ENTCdwW50iNFZmx97jYhpJGwy3ySspzXhApC0/kJYOdH8D0awhe2iFWSdrz
LHbI9rgbV2VaWxFPnNhVgxAxrNcf2LYCz5XHH1jUXx3kwOrkprXIYdqa3EcExEhS
ecmJHUDy7Np612Pg7QcoKWMfHSlHmh2JBPEcYwC+HVQsl62o11G2xivwCteEZlXp
tChmUfXLPtzlYYVXLjV3fFIZbBA9FeyYcxvKTwNjK/SjKwnfx8kEUA==
=dGiX
-----END PGP SIGNATURE-----
