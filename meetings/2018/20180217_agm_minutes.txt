-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Minutes
*********

Presence: Didier Raboud (OdyX, chari), Philipp Hug, Gaudenz Steinlin
(minutes), Raphael Walther, Iustin Pop, Salvatore Bonacorso, Robert Knight

Excused: Axel Beckert, Eva Ramon, Steve-David Marguet, Daniel Pocock,
Luca Capello

Date: February 17, 2018 18:00-

* Greetings
  Voting members present: 7

* Ratification of the minutes of the AGM 2017
  Agreed: 6

* Reports from the board

** President (Didier Raboud)
   There was a BSP in May and regular meetups in Zurich. Otherwise not
   much activity this year. Only small (117 CHF) merchandise sales.

** Treasurer (Philipp Hug)
   Two larger donations and several smaller ones. The total amount of
   donations is 5176.88 CHF, the total profit is 3032.35 CHF. The
   detailed financial statement is attached to the minutes.
   Agreed: 6
   Abstain: 1

* Discharge of the board
  Agreed: 7

* Approval of new members
  no new members applied since the last AGM

* Board election
  Candidates: Didier Raboud, Philipp Hug, Gaudenz Steinlin
  Agreed: 7

* Varia

** Raphael has a Debian banner from DebConf 13
   We decide it should go to someone who can use it better. We decided
   that it should go to Axel Beckert.

** Mailing list: We already decided to move the mailing list to Debian
   infrastructure but this did not happen. We decide that Didier will
   take care of this and try to get
   debian-switzerland@lists.debian.org as the list name. If the
   listmasters require another name, Didier is free to agree to a
   suitable name with them.

** @debian.ch Mail aliases: Philipp will take care to try to move them
   to Debian infrastructure. If that's not possible he will try to
   find another solution.

** BSP in 2018: We will hold a BSP on the Week-end of 1/2 December
   2018 at Aareheim in Bern. Gaudenz will organize the location and
   Didier will handle the invitation and coordination.

** Donations in Bitcoin: We will officially accept Bitcoin donations
   through our Bitcoin account and any donations will automatically be
   converted and sent to our account.

** Next AGM: 16 February 2019 in Lausanne. Organizer: Didier Raboud
-----BEGIN PGP SIGNATURE-----

iQEzBAEBCgAdFiEEXtjbd32AqFIO1HzsOrL5guAQm9UFAlqIbhoACgkQOrL5guAQ
m9ViNgf9FDtLCgqmyRVCMEEXpeIlwkBQQWYTVVAx/cvRLQ8ByAIv+u3y6ZDQKw6E
rnWVAaR9C4l4lumUcpsCAV6KCXW+xBhfGZV6GlAGxtd5kPmVVHiXMmTE6hDV55Ao
F1mm5CFWinZJPAXnBFrJCdLvhf/gFhAKx4+wq1v3wYBcYARkkLBwUpBDSgsM5qM5
qAtOMkTR2zBp6xOzzGSI7gkDb/XFJCsegHzihI5dis8rQMz0EMODvozMz7DXEd3i
BI3sZYiI/4fn9NCboYZGHGCLDOFIswp14ucXonp4N9dHCouzPCkqa3fH5OJuXmGW
DGTueX83Waruv/Wd6AYEPyGh+ekf2Q==
=qvFG
-----END PGP SIGNATURE-----
