Notes about procedures of the actuary
-------------------------------------

Duties:

  * Find a date for the Annual General Meeting
    by writing to community@lists.debian.ch.

  * Announce Annual General Meeting:
    >=2 weeks before time on both community@lists.debian.ch,
    debian-events-eu@lists.debian.org, debian-project@lists.debian.org

  * Keep notes for the protocol during the AGM.

  * Send protocol to the list at some point (?),
    with prior RFC to the board (?).
